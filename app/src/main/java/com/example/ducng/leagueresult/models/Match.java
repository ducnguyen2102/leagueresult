package com.example.ducng.leagueresult.models;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ducng on 02/28/2018.
 */

public class Match {
    int id;
    int homeTeamId;
    int awayTeamId;
    int homeTeamScore;
    int awayTeamScore;
    int result;

    public Match(int id, int homeTeamId, int awayTeamId, int homeTeamScore, int awayTeamScore) {
        this.id = id;
        this.homeTeamId = homeTeamId;
        this.awayTeamId = awayTeamId;
        this.homeTeamScore = homeTeamScore;
        this.awayTeamScore = awayTeamScore;
        if (homeTeamScore == awayTeamScore) {
            this.result = 0;
        } else {
            this.result = (homeTeamScore > awayTeamScore) ? 1 : -1;
        }
    }

    public Match(int id, int homeTeamId, int awayTeamId) {
        this.id = id;
        this.homeTeamId = homeTeamId;
        this.awayTeamId = awayTeamId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHomeTeamId() {
        return homeTeamId;
    }

    public void setHomeTeamId(int homeTeamId) {
        this.homeTeamId = homeTeamId;
    }

    public int getAwayTeamId() {
        return awayTeamId;
    }

    public void setAwayTeamId(int awayTeamId) {
        this.awayTeamId = awayTeamId;
    }

    public int getHomeTeamScore() {
        return homeTeamScore;
    }

    public void setHomeTeamScore(int homeTeamScore) {
        this.homeTeamScore = homeTeamScore;
    }

    public int getAwayTeamScore() {
        return awayTeamScore;
    }

    public void setAwayTeamScore(int awayTeamScore) {
        this.awayTeamScore = awayTeamScore;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public static List<Match> initMatches(List<Team> teams){
        List<Match> matches = new ArrayList<>();
        int numberOfTeams = teams.size();
        int id = 1;
        for (int i=0;i<numberOfTeams;i++){
            for (int j=i+1;j<numberOfTeams;j++){
                matches.add(new Match(id,teams.get(i).getId(),teams.get(j).getId(),teams.get(i).getId(),teams.get(j).getId()));
                id++;
                matches.add(new Match(id,teams.get(j).getId(),teams.get(i).getId(),teams.get(j).getId(),teams.get(i).getId()));
            }
        }
        return matches;
    }
    public static void showListMatches(List<Match> matches,List<Team> teams){
        for (int i=0;i<matches.size();i++){
            Log.e("ducnguyen","Match "+matches.get(i).getId()+" is "+teams.get(matches.get(i).getHomeTeamId()).getName()
            +" vs "+teams.get(matches.get(i).getAwayTeamId()).getName());
        }
    }
}
