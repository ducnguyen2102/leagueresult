package com.example.ducng.leagueresult.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.Toast;

import com.example.ducng.leagueresult.R;
import com.example.ducng.leagueresult.adapters.SampleTableAdapter;
import com.example.ducng.leagueresult.customview.TableFixHeaders;
import com.example.ducng.leagueresult.models.Match;
import com.example.ducng.leagueresult.models.Team;

import java.util.List;


public class MainActivity extends Activity {
    public List<Match> matches;
    public List<Team> teams;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.table);

        teams = Team.initTeams();
        matches = Match.initMatches(teams);
        TableFixHeaders tableFixHeaders = findViewById(R.id.table);
        tableFixHeaders.setMainActivity(this);
        tableFixHeaders.setAdapter(new MyAdapter(this));
    }

    public class MyAdapter extends SampleTableAdapter {

        private final int width;
        private final int height;

        MyAdapter(Context context) {
            super(context);

            Resources resources = context.getResources();

            width = resources.getDimensionPixelSize(R.dimen.table_width);
            height = resources.getDimensionPixelSize(R.dimen.table_height);
        }

        @Override
        public int getRowCount() {
            return 10;
        }

        @Override
        public int getColumnCount() {
            return 10;
        }

        @Override
        public int getWidth(int column) {
            return width;
        }

        @Override
        public int getHeight(int row) {
            return height;
        }

        @Override
        public String getCellString(int row, int column) {
            if (row == -1 && column == -1) {
                return "";
            } else if (row == -1) {
                return teams.get(column).getName();

            } else if (column == -1) {
                return teams.get(row).getName();
            } else {
                String score = "";
                for (int i = 0; i < matches.size(); i++) {
                    if (matches.get(i).getHomeTeamId() == row && matches.get(i).getAwayTeamId() == column) {
                        score = matches.get(i).getHomeTeamScore() + " - " + matches.get(i).getAwayTeamScore();
                    }
                }
                return score;
            }
        }

        @Override
        public int getLayoutResource(int row, int column) {
            final int layoutResource;
            switch (getItemViewType(row, column)) {
                case -1:
                    layoutResource = R.layout.item_table_blank;
                    break;
                case 0:
                    layoutResource = R.layout.item_table_header;
                    break;
                case 1:
                    layoutResource = R.layout.item_table;
                    break;
                default:
                    throw new RuntimeException("RuntimeException?");
            }
            return layoutResource;
        }

        @Override
        public int getItemViewType(int row, int column) {
            if (row == column) {
                return -1;
            } else if (row < 0 || column < 0) {
                return 0;
            } else {
                return 1;
            }
        }

        @Override
        public int getViewTypeCount() {
            return 3;
        }
    }

    public void addScore(int column, int row) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Edit Match Score")
                .setMessage("Edit Match Score between " + teams.get(row).getName() + " and " + teams.get(column).getName() + "?")
                .setPositiveButton("SAVE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(MainActivity.this, "Matched score has been updated!", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(MainActivity.this, "Update has been canceled!", Toast.LENGTH_SHORT).show();
                    }
                })
                .create().show();

    }
}
