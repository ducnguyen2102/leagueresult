package com.example.ducng.leagueresult.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ducng on 02/28/2018.
 */

public class Team {
    int id;
    String name;

    public Team(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public static List<Team> initTeams(){
        List<Team> teams = new ArrayList<>();
        teams.add(new Team(0,"Juventus"));
        teams.add(new Team(1,"Chelsea"));
        teams.add(new Team(2,"Arsenal"));
        teams.add(new Team(3,"Paris St-Germain"));
        teams.add(new Team(4,"Manchester City"));
        teams.add(new Team(5,"Bayern Munich"));
        teams.add(new Team(6,"Real Madrid"));
        teams.add(new Team(7,"Barcelona"));
        teams.add(new Team(8,"Manchester United"));
        teams.add(new Team(9,"Liverpool"));
        return teams;
    }
}
